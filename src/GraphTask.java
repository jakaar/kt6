import java.util.*;
import java.util.ArrayList;
import java.util.List;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 * kasutatud allikad:
 * https://www.geeksforgeeks.org/find-paths-given-source-destination/
 * https://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
 * https://slaystudy.com/hierholzers-algorithm/
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();

   }

   /** Actual main method to run examples and everything. */
   public void run() {

      Graph g = new Graph("G");

      //creating adjacency list
      ArrayList<ArrayList<Integer>> adjList = g.createRandomSimpleGraph(6, 6);

      //printing out the graph and path
      System.out.println(g);
      System.out.println("start: " + 2 + "  destination: " + 1);
      System.out.println("\n");
      System.out.println("Eliise Randmaa");
      Graph.printPath(adjList, 2 - 1, 1 - 1, 6);
      System.out.println("\n");
      System.out.println("Jan Johan Kaarnaväli");
      ArrayList<ArrayList<String>> testA = new ArrayList<>();
      System.out.println("Paths as vertices: " + g.depthFirst(g.first.next, g.first, testA));
      System.out.println("Paths as arcs: " + g.buildArcPaths(g.depthFirst(g.first.next, g.first, testA)));
      System.out.println("\n");
      System.out.println("Germo Linder");
      System.out.println("Euler cycle G1: " + g.findEulerCycle());
   }

   static class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private String info2; // this has the id of the vertex whats arc ends here
      private int height = getRandomInteger();

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      /**
       * Method to get a random int between the highest and the lowest point on Earth.
       * @return random int
       */
      private int getRandomInteger() {
         return (int) (Math.random() * ((8848 + 413) + 1)) - 413;
      }

      @Override
      public String toString() {
         return id;
      }
      public Iterator outEdges(){ // finds all the arcs that exit this vertex
         ArrayList<Arc> res = new ArrayList<>();
         Vertex a = this;
         if (a.first != null){
            res.add(a.first);
            Arc b = a.first;
            while (b.next != null){
               if (!res.contains(b.next)){
                  res.add(b.next);
                  b = b.next;
               }
            }
         }

         return res.iterator();
      }


      public boolean equals(Vertex v) {
         return this.id.equals(v.id);
      }


   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   static class Arc {

      private String id;
      private Vertex target;
      private Arc next;


      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }


   }


   static class Graph {

      private String id;
      private Vertex first;
      private final List<Vertex> vertices = new Stack<>();
      private final List<Arc> arcs = new Stack<>();
      int highestHeight = -414;
      private static Vertex highestVertex;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      /**
       * Collect all vertices into a List<Vertex> for better handling.
       */
      public void setVerticesList() {
         if (vertices.size() != 0) {
            vertices.clear();
         }
         Vertex v = first;
         while (v != null) {
            vertices.add(0, v);
            v = v.next;
         }
      }

      /**
       * Collect all arcs into a List<Arc> for better handling.
       */
      public void setArcsList() {
         if (arcs.size() != 0) {
            arcs.clear();
         }
         Vertex v = first;
         while (v != null) {
            Arc a = v.first;
            while (a != null) {
               arcs.add(0, a);
               a = a.next;
            }
            v = v.next;
         }
      }



//      @Override
//      public String toString() {
//         String nl = System.getProperty ("line.separator");
//         StringBuffer sb = new StringBuffer (nl);
//         sb.append (id);
//         sb.append (nl);
//         Vertex v = first;
//         while (v != null) {
//            sb.append (v.toString());
//            sb.append (" -->");
//            Arc a = v.first;
//            while (a != null) {
//               sb.append (" ");
//               sb.append (a.toString());
//               sb.append (" (");
//               sb.append (v.id);
//               sb.append ("->");
//               sb.append (a.target.id);
//               sb.append (")");
//               a = a.next;
//            }
//            sb.append (nl);
//            v = v.next;
//         }
//         return sb.toString();
//      }

      @Override
      public String toString() {


         String nl = System.getProperty("line.separator");
         StringBuilder sb = new StringBuilder(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {

            sb.append(v.toString());
            sb.append(" h=").append(v.height);
            if (v.height > highestHeight) {
               highestHeight = v.height;
               highestVertex = v;
            }
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         sb.append("Highest vertex is: ").append(highestVertex);
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public void createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + (n - i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                       + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                       + varray [vnr].toString(), varray [i], varray [vnr]);
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         int info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public ArrayList<ArrayList<Integer>> createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return null;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
         return Graph.toArraylists(connected);
      }

      /**
       * Traverse the graph depth-first.
       * param s source vertex and d is destination
       * param prevRes contains all the paths from a to b from previous iterations, at the beginning its empty.
       */
      public ArrayList<ArrayList<String>> depthFirst (Vertex s, Vertex d, ArrayList<ArrayList<String>> prevRes) {

         ArrayList<String> curPath = new ArrayList<String>();

         ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();

         List vq = Collections.synchronizedList (new LinkedList());
         vq.add (s);

         while (vq.size() > 0) {

            Vertex v = (Vertex)vq.remove (vq.size() - 1); // depth first == LIFO

            while (v.info2 != null && curPath.size() > 0 && !v.info2.equals(curPath.get(curPath.size() - 1))){
               curPath.remove(curPath.size() - 1);
            }

            if (!curPath.contains(v.id)){
               curPath.add(v.id);
            }

            Iterator eit = v.outEdges();
            while (eit.hasNext()) {
               Arc e = (Arc)eit.next();
               Vertex w = new Vertex(e.target.id, e.target.next, e.target.first);
               if (!curPath.contains(w.id)) {
                  w.info2 = v.id;
                  vq.add (w);
                  if (w.id.equals(d.id) ){

                     curPath.add(w.id);
                     if (!prevRes.contains(curPath)){

                        result.add(curPath);
                        result.addAll(prevRes);


                        ArrayList<ArrayList<String>> newList = depthFirst(s, d, result);
                        if (newList.isEmpty()){
                           return result;
                        }
                        for (ArrayList list: newList) {
                           if (!result.contains(list) && list.get(0).equals(s.id) && list.get(list.size() - 1).equals(d.id)){
                              result.add(list);
                           }
                        }
                        return result;
                     }else {
                        curPath.remove(curPath.size() - 1);
                        vq.remove(vq.size() - 1);

                     }

                  }
               }
            }
         }
         return result;
      }

      /**
       * Takes the result of depthFirst as a parameter and then builds a path of arcs from it.
       */
      public ArrayList<ArrayList<Arc>> buildArcPaths (ArrayList<ArrayList<String>> idPath){
         ArrayList<ArrayList<Arc>> arcPaths = new ArrayList<>();
         setVerticesList();
         for (ArrayList path: idPath) {
            ArrayList<Vertex> vertexPath = new ArrayList<>();
            ArrayList<Arc> arcPath = new ArrayList<>();
            for (int i = 0; i < path.size(); i++) {
               for (Vertex v: vertices) {
                  if (path.get(i).equals(v.id)){
                     vertexPath.add(v);
                  }
               }
            }
            for (int i = 0; i < vertexPath.size(); i++) {
               Iterator iter = vertexPath.get(i).outEdges();
               while (iter.hasNext()){
                  Arc a = (Arc)iter.next();
                  if (vertexPath.size() > i + 1){
                     if (a.target.id.equals(vertexPath.get(i + 1).id)){
                        arcPath.add(a);
                        break;
                     }
                  }
               }
            }
            arcPaths.add(arcPath);
         }
         return arcPaths;
      }

      /**
       * Find how many indegrees given vertex has.
       * @param vertex given vertex
       * @return number of indegrees
       */
      public int findInDegree(Vertex vertex) {
         if (vertices.size() == 0) {
            throw new RuntimeException("Run setVerticesList() first. (vertices list cant be empty)");
         }
         int inDegrees = 0;

         for (Vertex value : vertices) {
            Arc a = value.first;
            while (a != null) {
               if (a.target.equals(vertex)) {
                  inDegrees++;
               }
               a = a.next;
            }
         }
         return inDegrees;
      }

      /**
       * Find how many outdegrees given vertex has.
       * @param vertex given vertex
       * @return number of outdegrees
       */
      public int findOutDegree(Vertex vertex) {
         int outDegrees = 0;
         Arc a = vertex.first;
         while (a != null) {
            outDegrees++;
            a = a.next;
         }
         return outDegrees;
      }

      /**
       * Checks if given graph has eulerian cycle. If it does, run printEulerCycle function. Otherwise throws exception.
       * source: https://slaystudy.com/hierholzers-algorithm/
       */
      public List<Arc> findEulerCycle() {
         setVerticesList();
         setArcsList();
         try {
            if (vertices.size() == 0 || arcs.size() == 0) {
               throw new RuntimeException(String.format("graph can't have 0 vertices or arcs." +
                       " vertices: %s, arcs: %s in graph %s", vertices.size(), arcs.size(), id));
            }
            int startIndex = 0;
            int biggestDegree = 0;
            for (int i = 0; i < vertices.size(); i++) {

               int inD = findInDegree(vertices.get(i));
               int outD = findOutDegree(vertices.get(i));

               if (inD == outD && outD != 0) {
                  if (outD > biggestDegree) {
                     biggestDegree = outD;
                     startIndex = i;
                  }
               } else {
                  throw new RuntimeException(String.format("indegree and outdegree of each vertex has to be equal." +
                          " inD = %s, outD = %s for vertex %s in graph %s", inD, outD, vertices.get(i), id));
               }
            }

            return printEulerCycle(startIndex);
         } catch (RuntimeException e) {
            System.out.println(e);
         }

         return null;
      }


      /**
       * finds euler cycle with Hierholzer's algorithm (linear time complexity)
       * source: https://slaystudy.com/hierholzers-algorithm/
       * @param start starting index
       * @return List of arcs in right order
       */
      private List<Arc> printEulerCycle(int start) {

         Stack<Arc> cpath = new Stack<>();
         Stack<Arc> epath = new Stack<>();

         cpath.push(vertices.get(start).first); //push first arc to cpath

         while(!cpath.empty()){

            Arc u = cpath.peek(); //top of cpath
            Vertex v = u.target;

            if (cpath.contains(v.first) || epath.contains(v.first)) {
               v.first = v.first.next; //if arc already in a path, select the next one (for vertices with >1 outdegrees)
            }

            if (v.first != null) {

               cpath.push(v.first);
            } else {
               epath.add(0, u);
               cpath.pop();
            }
         }

         return epath;
      }

      //-----------------------------------------------------------------------

      /**
       * Method to print out the path from start to destination that goes through the highest vertex.
       * Calls out breathFirstSearch method.
       * If the start or the destination is not the highest vertex breathFirstSearch is called twice.
       * @param adjacencyList list of connected vertices
       * @param start satrting point
       * @param destination ending point
       * @param vertices number of vertices
       */
      public static void printPath(ArrayList<ArrayList<Integer>> adjacencyList, int start, int destination, int vertices) {

         LinkedList<Arc> path = new LinkedList<>();
         int[] predecessorList = new int[vertices];
         int[] distance = new int[vertices];
         int highestVertexnr = Integer.parseInt(highestVertex.id.replaceAll("\\D+", "")) -1 ;
         if (vertices<=start){
            throw new RuntimeException("Starting point should not be bigger than the number of vertices!!");
         }if (vertices<=destination){
            throw new RuntimeException("Destination should not be bigger than the number of vertices!!");

         }


         if (start == (highestVertexnr)) {
            if (!(breadthFirstSearch(adjacencyList, start, destination, vertices, predecessorList, distance))) {
               System.out.println("There is no path from " + start + " to " + destination + " that goes through the highest vertex " + highestVertex);
               return;
            }
            int current = destination;
            path.add(new Arc(Integer.toString(current)));
            while (predecessorList[current] != -1) {
               path.add(new Arc(Integer.toString(predecessorList[current])));
               current = predecessorList[current];
            }

         } else if (destination == (highestVertexnr)) {
            if (!(breadthFirstSearch(adjacencyList, start, highestVertexnr, vertices, predecessorList, distance))) {
               System.out.println("There is no path from " + start + " to " + destination + " that goes through the highest vertex " + highestVertex);
               return;
            }
            int current = highestVertexnr;
            path.add(new Arc(Integer.toString(current)));
            while (predecessorList[current] != -1) {
               path.add(new Arc(Integer.toString(predecessorList[current])));
               current = predecessorList[current];
            }

         } else {
            if (!breadthFirstSearch(adjacencyList, highestVertexnr, destination, vertices, predecessorList, distance)) {
               System.out.println("There is no path from " + start + " to " + destination + " that goes through the highest vertex " + highestVertex);
               return;
            }
            int current = destination;
            path.add(new Arc(Integer.toString(current)));
            while (predecessorList[current] != highestVertexnr) {
               path.add(new Arc(Integer.toString(predecessorList[current])));
               current = predecessorList[current];
            }


            if (!breadthFirstSearch(adjacencyList, start, highestVertexnr, vertices, predecessorList, distance)) {
               System.out.println("There is no path from " + start + " to " + destination + " that goes through the highest vertex " + highestVertex);
               return;
            }
            int current2 = highestVertexnr;
            path.add(new Arc(Integer.toString(current2)));
            while (predecessorList[current2] != -1) {
               path.add(new Arc(Integer.toString(predecessorList[current2])));
               current2 = predecessorList[current2];
            }
         }

         System.out.println("Path is :");
         for (int i = path.size() - 1; i >= 0; i--) {
            int ans = Integer.parseInt(path.get(i).id) + 1;
            System.out.print(ans);
            if (i > 0) {
               System.out.print(" -> ");
            }

         }
      }

      /**
       * breadthFirstSearch method to go through the graph and find the path from start to destination.
       * @param adjacencyList list of connected vertices
       * @param start starting point
       * @param destination destination
       * @param vertices number of vertices
       * @param pred list of ints that contains the info about predecessors
       * @param dist list of distances from starting point
       * @return boolean if the vertices are connected
       */
      private static boolean breadthFirstSearch(ArrayList<ArrayList<Integer>> adjacencyList,
                                                int start, int destination, int vertices, int[] pred, int[] dist){
         LinkedList<Integer> queue = new LinkedList<>();
         boolean[] visited = new boolean[vertices];

         for (int i = 0; i < vertices; i++) {
            visited[i] = false;
            dist[i] = Integer.MAX_VALUE;
            pred[i] = -1;
         }

         visited[start] = true;
         dist[start] = 0;
         queue.add(start);

         while (!queue.isEmpty()) {
            int j = queue.remove();
            for (int i = 0; i < adjacencyList.get(j).size(); i++) {
               int current = adjacencyList.get(j).get(i);
               if (!visited[current]) {
                  visited[current] = true;
                  dist[current] = dist[j] + 1;
                  pred[current] = j;
                  queue.add(current);

                  if (current == destination) {
                     return true;
                  }
               }
            }
         }
         return false;

      }


      /**
       * method to convert adjacency matrix to adjacency list, that it would be easily readable.
       * @param adj adjacency matrix
       * @return adjacency list
       */
      public static ArrayList<ArrayList<Integer>> toArraylists(int[][] adj) {
         ArrayList<ArrayList<Integer>> lists = new ArrayList<>();
         for (int[] ints : adj) {
            ArrayList<Integer> list = new ArrayList<>();
            for (int i = 0; i < ints.length; i++) {
               if (ints[i] == 1) {
                  list.add(i);
               }
            }
            lists.add(list);
         }
         return lists;
      }

   }
}
